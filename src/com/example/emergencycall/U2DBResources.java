package com.example.emergencycall;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class U2DBResources {
	public static final String TAG = "SQLite Database";
	private SQLiteDatabase database;
	private UserDB2 dbHelper;
	private String[] allColumns = { UserDB2.COLUMN_ID,
			UserDB2.COLUMN_NAME, UserDB2.COLUMN_NUMBER};

	public U2DBResources(Context context) {
		dbHelper = new UserDB2(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public U2 insertToU2(U2 u2) {
		ContentValues values = new ContentValues();
		values.put(UserDB2.COLUMN_NAME, u2.getName());
		values.put(UserDB2.COLUMN_NUMBER, u2.getNumber());
		long insertId = database.insert(UserDB2.TABLE_U2, null,
				values);

		Cursor cursor = database.query(UserDB2.TABLE_U2, allColumns,
				UserDB2.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		return cursorToU2(cursor);
	}

	public void deleteU2(U2 u2) {
		long id = u2.getId();
		Log.d(TAG, "Item deleted with id: " + id);
		database.delete(UserDB2.TABLE_U2, UserDB2.COLUMN_ID
				+ " = " + id, null);
	}

	public List<U2> getU2() {
		List<U2> comments = new ArrayList<U2>();
		Cursor cursor = database.query(UserDB2.TABLE_U2, allColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			U2 comment = cursorToU2(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		cursor.close();
		return comments;
	}

	private U2 cursorToU2(Cursor cursor) {
		U2 u2 = new U2();
		u2.setId(cursor.getLong(0));
		u2.setName(cursor.getString(1));
		u2.setNumber(cursor.getString(2));
		return u2;
	}

}
