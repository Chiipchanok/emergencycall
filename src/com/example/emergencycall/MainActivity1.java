package com.example.emergencycall;

import java.util.List;

//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class MainActivity1 extends ListActivity implements OnClickListener {

	final static String TAG = "SQLite";
	private U1DBResources datasource;
	List<U1> values;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_activity1);
		setTitle("Group 1");
		Button add = (Button) findViewById(R.id.anew);
		add.setOnClickListener(this);

		datasource = new U1DBResources(this);
		datasource.open();

		showDetail();

	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		showDetail(position);
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(state);
	}

	private void showDetail() {
		// TODO Auto-generated method stub
		values = datasource.getU1();
		ArrayAdapter<U1> adapter = new ArrayAdapter<U1>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity1, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		addForUser1(); 
	}

	private void showDetail(final int id) {
		// Display details of each item.
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final ArrayAdapter<U1> adapter = (ArrayAdapter<U1>) getListAdapter();
		final U1 u1 = (U1) getListAdapter().getItem(id);

		final Dialog dialog = new Dialog(MainActivity1.this);
		dialog.setContentView(R.layout.detail);
		dialog.setTitle("Details");
		dialog.setCancelable(true);

		TextView name1 = (TextView) dialog.findViewById(R.id.u1dtname);
		TextView num1 = (TextView) dialog.findViewById(R.id.u1dtnum);

		name1.setText(" Name :\t" + u1.getName());
		num1.setText(" Number :\t" + u1.getNumber());
		final String callnum = u1.getNumber();

		//Call
		 Button cl = (Button) dialog.findViewById(R.id.button_Call);
		 cl.setOnClickListener(new OnClickListener(){
			 public void onClick(View v){
				 Intent it = new Intent(Intent.ACTION_CALL);
	    			it.setData(Uri.parse("tel:" +callnum));
	    			startActivity(it);
			 }
		 });
		
		// delete
		Button del = (Button) dialog.findViewById(R.id.button_Delete);
		del.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					datasource.deleteU1(u1);
					adapter.remove(u1);
					dialog.dismiss();
					Toast.makeText(MainActivity1.this, "Deleted",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		// close
		Button c = (Button) dialog.findViewById(R.id.button_Close);
		c.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}
	
	public void addForUser1() {
		
		final Dialog dialog = new Dialog(MainActivity1.this);
																
		dialog.setContentView(R.layout.edit_add);
		dialog.setTitle("Add a new call");
		dialog.setCancelable(true);
		
		Button s = (Button) dialog.findViewById(R.id.save);
		s.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// get string
				EditText name = (EditText) dialog.findViewById(R.id.u1name);
				EditText number = (EditText) dialog.findViewById(R.id.u1num);

				String values_name = name.getText().toString();
				String values_number = number.getText().toString();
				@SuppressWarnings("unchecked")
				ArrayAdapter<U1> adapter = (ArrayAdapter<U1>) getListAdapter();
				U1 u1 = new U1();
				u1.setName(values_name);
				u1.setNumber(values_number);
				u1 = datasource.insertU1(u1);
														
				adapter.add(u1);
				dialog.cancel();
			}
		});
		Button c = (Button) dialog.findViewById(R.id.button_Cancel);
		c.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		datasource.close();
		super.onPause();
	}
	
}
