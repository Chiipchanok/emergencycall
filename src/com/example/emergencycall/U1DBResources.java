package com.example.emergencycall;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class U1DBResources {

	public static final String TAG = "SQLite Database";
	private SQLiteDatabase database;
	private UserDB1 dbHelper;
	private String[] allColumns = { UserDB1.COLUMN_ID,
			UserDB1.COLUMN_NAME, UserDB1.COLUMN_NUMBER};

	public U1DBResources(Context context) {
		dbHelper = new UserDB1(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public U1 insertU1(U1 u1) {
		ContentValues values = new ContentValues();
		values.put(UserDB1.COLUMN_NAME, u1.getName());
		values.put(UserDB1.COLUMN_NUMBER, u1.getNumber());
		long insertId = database.insert(UserDB1.TABLE_U1, null,
				values);

		Cursor cursor = database.query(UserDB1.TABLE_U1, allColumns,
				UserDB1.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		return cursorToU1(cursor);
	}

	public void deleteU1(U1 u1) {
		long id = u1.getId();
		Log.d(TAG, "Item deleted with id: " + id);
		database.delete(UserDB1.TABLE_U1, UserDB1.COLUMN_ID
				+ " = " + id, null);
	}

	public List<U1> getU1() {
		List<U1> comments = new ArrayList<U1>();
		Cursor cursor = database.query(UserDB1.TABLE_U1, allColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			U1 comment = cursorToU1(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		cursor.close();
		return comments;
	}

	private U1 cursorToU1(Cursor cursor) {
		U1 u1 = new U1();
		u1.setId(cursor.getLong(0));
		u1.setName(cursor.getString(1));
		u1.setNumber(cursor.getString(2));
		return u1;
	}


}
