package com.example.emergencycall;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UserDB2 extends SQLiteOpenHelper{
	public static final String TABLE_U2 = "User2";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_NUMBER = "number";
	private static final String DATABASE_NAME = "U2.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table "
			+ TABLE_U2 + "( " + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_NAME + " text,"
			+ COLUMN_NUMBER + " text" + ");";

	public UserDB2(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
		Log.w(UserDB2.class.getName(), "Upgrading database from version "
				+ oldV + " to " + newV + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_U2);
		onCreate(db);
	}
}
