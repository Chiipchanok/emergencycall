package com.example.emergencycall;

import java.util.List;

//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class MainActivity2 extends ListActivity implements OnClickListener {

	final static String TAG = "SQLite";
	private U2DBResources datasource;
	List<U2> values;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_activity2);
		setTitle("Group 2");
		Button add2 = (Button) findViewById(R.id.anew2);
		add2.setOnClickListener(this);

		datasource = new U2DBResources(this);
		datasource.open();

		showDetail();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		showDetail(position);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity2, menu);
		return true;
	}

	private void showDetail() {
		// TODO Auto-generated method stub
		values = datasource.getU2();
		ArrayAdapter<U2> adapter = new ArrayAdapter<U2>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		addForUser2();
	}
	private void showDetail(final int id) {
		// Display details of each item.
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final ArrayAdapter<U2> adapter = (ArrayAdapter<U2>) getListAdapter();
		final U2 u2 = (U2) getListAdapter().getItem(id);

		final Dialog dialog = new Dialog(MainActivity2.this);
		dialog.setContentView(R.layout.detail);
		dialog.setTitle("Details");
		dialog.setCancelable(true);

		TextView name2 = (TextView) dialog.findViewById(R.id.u1dtname);
		TextView num2 = (TextView) dialog.findViewById(R.id.u1dtnum);

		name2.setText(" Name :\t" + u2.getName());
		num2.setText(" Number :\t" + u2.getNumber());
		final String callnum = u2.getNumber();

		//Call
		 Button cl = (Button) dialog.findViewById(R.id.button_Call);
		 cl.setOnClickListener(new OnClickListener(){
			 public void onClick(View v){
				 Intent it = new Intent(Intent.ACTION_CALL);
	    			it.setData(Uri.parse("tel:" +callnum));
	    			startActivity(it);
			 }
		 });
		
		// delete
		Button del = (Button) dialog.findViewById(R.id.button_Delete);
		del.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (getListAdapter().getCount() > 0) {
					datasource.deleteU2(u2);
					adapter.remove(u2);
					dialog.dismiss();
					Toast.makeText(MainActivity2.this, "Deleted",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		// close
		Button c = (Button) dialog.findViewById(R.id.button_Close);
		c.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}
	
	public void addForUser2() {
		// Insert new data into database.
		final Dialog dialog = new Dialog(MainActivity2.this);
																
		dialog.setContentView(R.layout.edit_add);
		dialog.setTitle("Add a new call");
		dialog.setCancelable(true);
		
		Button s = (Button) dialog.findViewById(R.id.save);
		s.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// get string
				EditText name = (EditText) dialog.findViewById(R.id.u1name);
				EditText number = (EditText) dialog.findViewById(R.id.u1num);

				String values_name = name.getText().toString();
				String values_number = number.getText().toString();
				@SuppressWarnings("unchecked")
				ArrayAdapter<U2> adapter = (ArrayAdapter<U2>) getListAdapter();
				U2 u2 = new U2();
				u2.setName(values_name);
				u2.setNumber(values_number);
				u2 = datasource.insertToU2(u2);												
				adapter.add(u2);
				dialog.cancel();
			}
		});
		Button c = (Button) dialog.findViewById(R.id.button_Cancel);
		c.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		datasource.close();
		super.onPause();
	}
	
}
