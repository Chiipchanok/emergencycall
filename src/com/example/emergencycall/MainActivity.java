package com.example.emergencycall;

import android.net.Uri;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;



	
	public class MainActivity extends ListActivity{
		String[]MainMenu = {"Emergency","Taxi","Utility","Bank","Food"};
		String[]Sub1 = new String[]{"Taxi","Return"};
		String[]Sub2 = new String[]{"Electricity","Water Supply","Return"};
		String[]Sub3 = new String[]{"Kasikorn","KTB","SCB","TMB","Return"};
		String[]Sub4 = new String[]{"KFC","Pizza","McDonald","Return"};
		
		String strMain = "";
		ArrayAdapter<String> mainAdt;
		ArrayAdapter<String> subAdt1;
		ArrayAdapter<String> subAdt2;
		ArrayAdapter<String> subAdt3;
		ArrayAdapter<String> subAdt4;
		
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainAdt = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, MainMenu);
        subAdt1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Sub1);
        subAdt2 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Sub2);
        subAdt3 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Sub3);
        subAdt4 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Sub4);
        setListAdapter(mainAdt);
    }
    
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	if(getListAdapter()==mainAdt){
    		strMain = getListView().getItemAtPosition(position).toString();
    		if(strMain.trim()=="Emergency"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:191"));
    			startActivity(it);
    		}else{
				if (strMain.trim()=="Taxi"){
					setListAdapter(subAdt1);
				}else{
					if(strMain.trim()=="Utility"){
						setListAdapter(subAdt2);
					}else{
						if (strMain.trim()=="Bank"){
							setListAdapter(subAdt3);
						}else{
							if (strMain.trim()=="Food"){
								setListAdapter(subAdt4);
    			}
    		}
    	}
    }
}
    		}else{
    		String strSub = getListView().getItemAtPosition(position).toString();
    		if(strSub=="Return"){
    			setListAdapter(mainAdt);
    		}else if(strSub=="Electricity"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1130"));
    			startActivity(it);
    		}else if(strSub=="Water Supply"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1125"));
    			startActivity(it);
    		}else if(strSub=="KTB"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1333"));
    			startActivity(it);
    		}else if(strSub=="SCB"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:027777777"));
    			startActivity(it);
    		}else if(strSub=="TMB"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1551"));
    			startActivity(it);
    		}else if(strSub=="Kasikorn"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:02888888"));
    			startActivity(it);
    		}else if(strSub=="KFC"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1150"));
    			startActivity(it);
    		}else if(strSub=="Pizza"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1112"));
    			startActivity(it);
    		}else if(strSub=="McDonald"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1711"));
    			startActivity(it);
    		}else if(strSub=="Taxi"){
    			Intent it = new Intent(Intent.ACTION_CALL);
    			it.setData(Uri.parse("tel:1681"));
    			startActivity(it);
    	}
    }
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.action_user1){
			Intent i = new Intent(this,MainActivity1.class);
		startActivity(i);
		}
		else if (id == R.id.action_user2){
			Intent i = new Intent(this,MainActivity2.class);
			startActivity(i);
		}
		return super.onOptionsItemSelected(item);
		
	}
    
}
