package com.example.emergencycall;

public class U2 {
	private long id;
	private String name;
	private String number;

	public long getId() {
		// Get id.
		return id;
	}

	public void setId(long id) {
		// Set id.
		this.id = id;
	}

	public String getName() {
		// Get Name.
		return name;
	}

	public void setName(String name) {
		// Set name.
		this.name = name;
	}

	public String getNumber() {
		// Get number.
		return number;
	}

	public void setNumber(String number) {
		// Set number.
		this.number = number;
	}

	@Override
	public String toString() {
		return name;
	}
}
